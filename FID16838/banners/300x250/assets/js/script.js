/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------

        tl.add('frame1')
            .from('.text1', 1, { y:"-100%", ease: Power4.easeInOut }, 'frame1+=0.5')
            .from('.text2', 1, { y:"-100%", ease: Power4.easeInOut }, 'frame1+=0.5')
           
          ;

        // ---------------------------------------------------------------------------

        tl.add('frame2', '+=1')
        .to('.bg1', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame2+=0.0')
        .to('.text1', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame2+=0')
        .to('.text2', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame2+=0')
        .from('.text3', 1, { y:"-100%", ease: Power4.easeInOut }, 'frame2+=1')
        .from('.text4', 1, { y:"-100%", ease: Power4.easeInOut }, 'frame2+=1')
          ;

        // ---------------------------------------------------------------------------

        tl.add('frame3', '+=1')
        .to('.bg2', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame3+=0.0')
        .to('.text3', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame3+=0')
        .to('.text4', 0.8, { y:"100%", ease: Power4.easeInOut }, 'frame3+=0')
        .from('.bo-logo', 0.5, { x:"100%", ease: Power4.easeInOut }, 'frame3+=1')
        .from('.logo', 0.5, { x:"100%", ease: Power4.easeInOut }, 'frame3+=1.5')
        .from('.cta', 0.5, { x:"100%", ease: Power4.easeInOut }, 'frame3+=2')
        .from('.fdic', 0.5, { x:"100%", ease: Power4.easeInOut }, 'frame3+=2.5')
          ;


        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
